
#!/bin/sh

TS=$(date +%F_%s)
CURRENT_SV_FB="/$(btrfs sub get-default /.base | rev | cut -d' ' -f 1 | rev)"
CURRENT_SV="/.base$CURRENT_SV_FB"

NEW_SV_FB="/.subvolumes/ROOT_$TS"
NEW_SV="/.base$NEW_SV_FB"

CURRENT_SV_HOME_FB="$(cat $CURRENT_SV/etc/fstab | grep -oP 'subvol=/.subvolumes/HOME[-_0-9]*' | cut -f2 -d=)"
NEW_SV_HOME_FB="/.subvolumes/HOME_$TS"

CURRENT_SV_HOME="/.base$CURRENT_SV_HOME_FB"
NEW_SV_HOME="/.base$NEW_SV_HOME_FB"

SUBSTITUTE="s@$CURRENT_SV_FB@$NEW_SV_FB@g"

waitloop () {
  read -r -p "finished ? [y/N] : " response2
  while [[ "$response2" != "y" ]] ; do
    bash
    read -r -p "finished ? [y/N] : " response2
  done
  return 0
}

generategrubcfg () {
  echo -e "
insmod part_gpt
insmod part_msdos
if [ -s \$prefix/grubenv ]; then
  load_env
fi

function load_video {
  if [ x\$feature_all_video_module = xy ]; then
    insmod all_video
  else
    insmod efi_gop
    insmod efi_uga
    insmod ieee1275_fb
    insmod vbe
    insmod vga
    insmod video_bochs
    insmod video_cirrus
  fi
}

font=unicode
if loadfont \$font ; then
  set gfxmode=auto
  load_video
  insmod gfxterm
  set locale_dir=\$prefix/locale
  set lang=en_US
  insmod gettext
fi
terminal_input console
load_video
set gfxpayload=keep
echo 'loading modules ...'
insmod gzio
insmod btrfs
insmod luks2
echo 'opening crypted ...'
cryptomount -a
echo 'set root ...'
set root=$FSUUID
set SUBV=$NEW_SV_FB
echo 'searching for kernel ...'
search --no-floppy --fs-uuid --set=root \$root
echo	'Loading Linux linux-hardened ...'
linux	\$SUBV/boot/vmlinuz-linux-hardened lsm=landlock,lockdown,yama,apparmor,bpfmicrnm l1tf=full,force mds=full,nosmt mitigations=auto,nosmt nosmt=force lockdown=confidentiality
echo 'Loading initial ramdisk ...'
initrd	\$SUBV/boot/intel-ucode.img \$SUBV/boot/initramfs-linux-hardened.img
boot
" > /tmp/newgrubcfg
return 0
}

generatefstab () {
 echo -e " 
 #ROOT
 UUID=$FSUUID / btrfs rw,relatime,space_cache=v2,subvol=$NEW_SV_FB 0 0
 #EFI 
 UUID=$EFIUUID /efi vfat rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro 0 2 
 #BASE
 UUID=$FSUUID /.base btrfs rw,relatime,space_cache=v2,subvolid=5,subvol=/ 0 0
 #HOME
 UUID=$FSUUID /.overlays/lower/home btrfs ro,relatime,space_cache=v2,subvol=$NEW_SV_HOME_FB 0 0
 UUID=$FSUUID /varhome btrfs rw,relatime,space_cache=v2,subvolid=261,subvol=/.subvolumes/VARHOME 0 0
 UUID=$FSUUID /vault btrfs rw,relatime,space_cache=v2,subvolid=260,subvol=/.subvolumes/VAULT 0 0
 #SWAP
 UUID=$FSUUID /.swap btrfs rw,relatime,space_cache=v2,subvolid=262,subvol=/.subvolumes/SWAP 0 0
 /.base/.subvolumes/SWAP/swapfile none swap defaults 0 0
 #TEMP FS
 tmpfs /.overlays/upper/home tmpfs rw 0 0
 #OVERLAYS
 overlay /home overlay noauto,x-systemd.automount,lowerdir=/.overlays/lower/home,upperdir=/.overlays/upper/home/home,workdir=/.overlays/upper/home/work 0 0" > /tmp/newfstab
  return 0
}

echo "Starting system atomic update" &&
echo "Current subvolume is $CURRENT_SV" &&
echo "Current home is $CURRENT_SV_HOME" &&
echo "new volume will be $NEW_SV" &&
echo "new home will be $NEW_SV_HOME" &&

read -r -p  "confirm ? [y/N] : " response

if [[ "$response" = "y" ]] ; then
  echo "starting update" &&

  echo "snapshotting ROOT" &&
  btrfs sub snap $CURRENT_SV $NEW_SV &&
  echo "snapshotting HOME" &&
  btrfs sub snap $CURRENT_SV_HOME $NEW_SV_HOME &&
  chmod 755 $NEW_SV &&

  echo "mounting runtime" &&
  mount --mkdir --bind / $NEW_SV/.current_temp_root &&
  mount --mkdir --bind /home $NEW_SV/.current_temp_home &&
  mount --mkdir -t proc /proc $NEW_SV/proc &&
  mount --mkdir --bind /dev $NEW_SV/dev &&
  mount --mkdir --bind /run $NEW_SV/run &&
  mount --mkdir -t sysfs /sys $NEW_SV/sys &&
  mount --mkdir --bind /efi $NEW_SV/efi &&
  mount --mkdir --bind /tmp $NEW_SV/tmp &&
  mount --mkdir --bind /varhome $NEW_SV/varhome &&
  mount /dev/disk/by-uuid/$FSUUID -o subvol=$NEW_SV_HOME_FB $NEW_SV/home &&
  
  echo "successfully mounted runtime" &&

  echo "you will now be chrooted in the new root to perform the change" &&
  EVOLVING=TRUE chroot $NEW_SV

  echo "will now edit the FS structure, exit the shell to abort" &&
  waitloop &&
  umount $NEW_SV/.current_temp_root &&
  umount $NEW_SV/.current_temp_home &&
  umount $NEW_SV/proc &&
  umount $NEW_SV/dev &&
  umount $NEW_SV/run &&
  umount $NEW_SV/sys &&
  umount $NEW_SV/tmp &&
  umount $NEW_SV/efi &&
  umount $NEW_SV/varhome &&
  umount $NEW_SV/home &&
  rmdir $NEW_SV/.current_temp_root &&
  rmdir $NEW_SV/.current_temp_home &&

  echo "Generating the new fstab and the new grub cfg :" &&
  generatefstab &&
  cat /tmp/newfstab &&
  generategrubcfg &&
  cat /tmp/newgrubcfg &&
  echo "Please check if everything is ok" &&
  waitloop &&

  echo "branching to new root" &&
  
  echo "setting new default subvolume" &&
  btrfs sub set-default $NEW_SV && 
  echo "successfully set new default subvolume :" &&
  btrfs sub get-default /.base &&

  echo "set new grub" &&
  cat /efi/grub/grub.cfg > /tmp/grub.cfg.old &&
  cat /efi/grub/grub.cfg > /efi/grub/grub.cfg.old &&
  cat /tmp/newgrubcfg > /efi/grub/grub.cfg &&
  echo "wrote new grub cfg" &&

  echo "set new fstab" &&
  cat /etc/fstab > /tmp/fstab.old &&
  cat /tmp/newfstab > $NEW_SV/etc/fstab &&
  echo "wrote new fstab" &&

  echo "ROOT FILESYSTEM SUCCESSFULLY EVOLVED" &&
  exit 0 ||
  echo "error occure, rolling back..." &&
  btrfs sub set-default $CURRENT_SV
  if [ -e /tmp/grub.cfg.old ] ; then
    cat /tmp/grub.cfg.old > /efi/grub/grub.cfg
  fi
  exit 1
else echo "aborted"
fi
