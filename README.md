# Brumaire Little Scripts

Collection de petits scripts nuls, essentiellement pour mon usage personnel, que je partage dans l'éventualité ou ça pourraît servir à quelqu'un d'autre.

J'en ai plein à refourguer ici, je vais essayer d'en publier un par jour jusqu'à épuisement des stocks

## Les scripts

### breath.sh

Timer pour exercice de respiration.

Vous pouvez définir les différents exercices dans un fichier de config, le
format est le suivant :

Configurez la variables d'environnement BREATH_FILE,
puis lancez le script avec en premier argument l'exercice que vous voulez
réaliser : `./breathe.sh basic`.

C'est aussi simple que cela.

```
# <name> <inhale time> <exhale time> <duration>
basic 4 6 60
```

### webmarks-rofi-2.sh

Au départ conçu comme un simple gestionnaire de bookmarks,
il s'agit maintenant d'une sorte de launcher universel, capable de lancer applications comme bookmarks.

#### Utilisation

À bind à un raccourci clavier, puis ensuite entrer la clef d'un bookmark ou d'un service (préfixé de ! dans ce dernier cas).

#### Dépendences

- rofi

#### Conf 

##### Environnement

- `SERVICES_FILE` : fichier contenant les services déclarés
- `BOOKMARKS_FILE` : fichier contenant les bookmarks
- `TERMGRUN` : Commande pour lancer une app cli ou tui dans l'environnement de bureau (ex : `alacritty -e bash -c`)

##### Fichiers (exemples)

###### SERVICE\_FILE

```
run!termapp <PARAM>
smallweb!termapp links <PARAM>
fullweb!guiapp firefox <PARAM>
gemini!termapp amfora <PARAM>
alex!smallweb alexandria.org/?q=<PARAM>
reddit!termapp tuir -s <PARAM>
wiki!fullweb en.wikipedia.org/w/index.php?search=<PARAM>
```

###### BOOKMARKS\_FILE

```
[ APP ]
mail = !run mutt
tasks = !sh task
mastodon = !run toot tui
htop = !run htop

[ BOOKMARKS ]
archwiki = !smallweb https://archlinux.org
reddit france = !reddit france
```

### journalEntry.sh

Un tout petit programme dont le but est d'aider à tenir un journal texte (markdown).
Il se configure un fichier comportant une série de sections et/ou de questions.

Quand vous appelez le script, il vous propose chacuns des éléments spécifier, vous pouvez choisir de passer, ou de répondre. Si vous répondez,il va ouvrir votre éditeur de texte TUI ou GUI défini par la variable d'environnement `EDITOR`.

À la fin, quand vous avez épuisé toutes les sections/questions, vous pourrez relire et retoucher la nouvelle entrée dans la globalité ou valider et écrire le fichier dans le répertoire spécifé.

#### Configuration

##### Environnemnt
- `JOURNAL_QUESTIONS_FILE_PATH` : Fichier comportant les sections ou questions à poser
- `JOURNAL_PATH` : Chemin absolu vers le répertoire ou écrire les entrées du journal

##### Fichier (exemple)

```
Comment te sens-tu aujourd'hui (et pourquoi) ?
Est-ce que tu as fait du sport ?
As-tu pris des nouvelles d'un proche ?
As-tu autre chose à ajouter ?
```

### sendFares.py

Un petit script pour vous envoyer par mail la liste de course depuis taskwarrior (tâches avec le tag `courses`) .

Comme ça quand vous partez en courses, vous lancez juste le script depuis votre PC, vous recevez la liste sur smartphone et hop, pas besoin de galérer 10 ans ou d'utiliser une appli dédiée pour le transfert.

#### Dépendences

- Taskwarrior
- librairies python : taskw, smtplib, ssl

#### Configuration

##### Variable à définir dans le script

- `SMTP_DOMAIN` : Domaine 
- `PORT` : port smtp (starttls)
- `SENDER` : id du compte smtp
- `PASS` : mot de passe compte smtp
- `FROM` : émetteur du mail
- `TO` : destinataire du mail

### evolve.sh

N'UTILISEZ PAS CE SCRIPT TEL QUEL, ne le lancez pas sur un système non configré pour, sous peine de dommages irréversibles.

Celui-là est un peu plus spécial que les autres. Il est issu de mon intérêt pour btrfs, ainsi que pour les systèmes immutables et est inspiré de distribution telles que NixOS, Suse Micro, Fedora Silverfish. J'ai développé cette solution plutôt que d'utiliser l'une des distributions précédentes car je voulais absolument conserver un système basé sur Archlinux, en raison du respect du principe KISS qu'offre cette dernière distribution. Je cherchais aussi à apprendre comment implémenter un tel système.

Par défaut, mon système est monté en read-only et exploite un overlayfs pour l'écriture. Toutes les modification au runtime sont écrites
en RAM.
L'intérêt pour moi est d'éviter que le système ne soit pollué avec le temps et de minimiser le temps passé à la maintenance. 

J'applique le même système à ma partition /home. En effet le $HOME est souvent aggressé de diverses manières par des programmes qui refusent d'être xdg-compliant et écrivent dedans de manière anarchique. J'utilise deux autres subvolumes /vault et /varhome, qui sont eux mutables et contiennent données personnelles d'une part, et des choses qui ont sont autorisées à muter dans l'autre. Pour reconstituer une arborescence propre dans /home, j'abuse des liens symboliques vers ces deux subvolumes. 

Quand je veux faire évoluer le système, j'appelle ce script qui va réaliser des snapshots btrfs des subvolumes protégés contre l'écriture (/ et /home dans mon cas), monter les subvolumes/snapshots et chroot sur la nouvelle racine en appelant un shell.

Dans le contexte de ce shell, je peux modifier le système (mettre à jour les packages, modifier ma config…). Une fois les modifications effectuées,
le script va générer le fstab de la nouvelle racine, et la nouvelle config du bootloader (grub) dans mon cas pour utiliser les nouveaux subvolumes btrfs.

De cette manière, toutes les modifications apportées au système sont atomiques et transactionelles. Si je veux revenir à un état passé, il suffit de changer le chemin vers le subvolume à monter dans la config GRUB.

ATTENTION, la configuration et l'installation initiale du système ont été faites à la mano, et ce script est conçu pour les spécificités de mon système uniquement (/ chiffré, /boot chiffré, config grub simplifiée et single boot, swapfile sur btrfs). Aussi vous pouvez vous en inspirer, mais ne l'utilisez pas tel quel, adaptez le à votre système, à votre configuration.

#### Configuration

##### Environnement

- `EFIUUID` : uuid de la partition efi comportant le bootloader
- `FSUUID` : uuid de la partition btrfs


### WatchLaundry.py

Ce petit script a permet :
- (1) de vérifier des une machine est disponible dans les laveries gad
- (2) d'attendre qu'une machine soit terminée et d'afficher une notification le cas
  échéant.

Le script a deux arguments obligatoire :
  - l'id de la laverie
  - le mot de passe associé (généralement fourni par votre résidence ou prestataire laverie)

Enfin, le troisième argument vous permet de spécifier un numéro de machine ou
séchoir, dans ce cas, c'est le comportement (2) qui sera mis en œuvre, pour vous
permettre par exemple d'attendre la fin de votre lavage, ou celui de quelqu'un
d'autre pour occuper la place ensuite.

Je l'ai utilisé fructueusement pendant + d'un an dans ma résidence, 
mais depuis le site ne retourne plus un état correct. Il m'a fait gagné beaucoup
de temps.

Vous devez faire attention à bien installer les dépendances python pour
l'utiliser.

Prenez garde aussi au fait que c'est un scrapper, et que c'est probablement
borderline légal.

Les notifications ne fonctionneront qu'en environnement unix disposant de
libnotify.
