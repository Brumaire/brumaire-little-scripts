#!/usr/bin/env bash

declare -a questions=()
mapfile -t questions < $JOURNAL_QUESTIONS_FILE_PATH

DATE="$(date +%Y-%m-%d-%Hh%Mm%Ss)"
FILE_NAME="$DATE.md"
FILE_PATH="$JOURNAL_PATH/$FILE_NAME"

TEMP_ENTRY_PATH="/tmp/newJournalEntry"
TEMP_ANSWER_PATH="/tmp/newJournalAnswer"


ask() {
  what=$1
  response=""
  edit=e
  confirm=n
  while [[ "$confirm" != "y" ]] ; do
    touch $TEMP_ANSWER_PATH
    read -rp "> $what [E/s] : " edit
    if [[ "$edit" == "s" ]] ; then
      rm $TEMP_ANSWER_PATH
      return 0
    fi
    $EDITOR $TEMP_ANSWER_PATH
    echo "## $what"
    echo ""
    cat $TEMP_ANSWER_PATH
    echo ""
    read -rp "Valider ? [y/N] : " confirm
    clear
  done
  response=$(cat $TEMP_ANSWER_PATH)
  rm $TEMP_ANSWER_PATH
  ###########################
  if [[ "$edit" != "s" ]] ; then 
    echo -ne "$TEMP_ENTRY
## $what

$response

" >> $TEMP_ENTRY_PATH
  fi
  ###########################
  return 0
}

touch $TEMP_ENTRY_PATH
echo ""
echo ">> Nouvelle entrée du journal identifiée $DATE <<"
echo "# $DATE" > $TEMP_ENTRY_PATH
echo ""

  for question in "${questions[@]}" ; do
    ask "$question"
  done

ACTION=n
while [[ "$ACTION" != "y" ]] ; do
  if [[ "$ACTION" = "q" ]] ; then
    rm $TEMP_ENTRY_PATH
    rm $TEMP_ACTION_PATH
    exit 0
  elif [[ "$ACTION" = "e" ]] ; then
    $EDITOR $TEMP_ENTRY_PATH
  fi
  echo ""
  echo ">> Nouvelle entrée du journal sera écrite <<"
  echo ""
  cat $TEMP_ENTRY_PATH
  read -rp "Valider ? [Y/e/q] : " ACTION
done

cp $TEMP_ENTRY_PATH $FILE_PATH && echo "entrée $FILE_PATH écrite" &&
rm $TEMP_ENTRY_PATH

