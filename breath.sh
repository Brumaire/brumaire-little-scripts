#!/usr/bin/sh


EX_TIME="6"
IN_TIME="4"
DURATION=60
COUNTER=0

#check for .breath file
if [[ -e $BREATH_FILE ]] ; then
    BREATH_FILE_CONTENT=`cat $BREATH_FILE | grep -P --invert-match ^#`
else
  echo "Breath file : $BREATH_FILE"
  echo "Please provide a breath.conf file with the BREATH_FILE env"
  exit 1
fi

SELECTED_MODE=`echo $BREATH_FILE_CONTENT | grep -P "^$1"`

if [[ $SELECTED_MODE == "" ]] ; then
  echo "Mode $1 not found in config file"
  exit 1
fi

#Get parameters
SEL_NAME=`echo $SELECTED_MODE | cut -d " " -f 1` 
IN_TIME=`echo $SELECTED_MODE  | cut -d " " -f 2` 
EX_TIME=`echo $SELECTED_MODE  | cut -d " " -f 3` 
DURATION=`echo $SELECTED_MODE | cut -d " " -f 4` 

clearPrint () {
  tput cup 0 0
  #echo "Time remaining : `expr $DURATION - $COUNTER`"
  echo $1
}

inhale () {
  IN_COUNTER=0
  while [[ $IN_COUNTER -lt $IN_TIME ]] ; do
    clearPrint "inhale... (`expr $IN_TIME - $IN_COUNTER`)"
    IN_COUNTER=`expr $IN_COUNTER + 1`
    COUNTER=`expr $COUNTER + 1`
    sleep 1s
  done
}

exhale () {
  EX_COUNTER=0
  while [[ $EX_COUNTER -lt $EX_TIME ]] ; do
    clearPrint "exhale... (`expr $EX_TIME - $EX_COUNTER`)"
    EX_COUNTER=`expr $EX_COUNTER + 1`
    COUNTER=`expr $COUNTER + 1`
    sleep 1s
  done
}


clear
echo -e "Selected exercise \"$SEL_NAME\" :"
echo -e "- Inhale time (s): $IN_TIME"
echo -e "- Exhale time (s): $EX_TIME"
echo -e "- Duration (s) : $DURATION"
echo -e "\n"
echo -e "Starting in 10 seconds with an exhale, Get ready !"
sleep 10s
clear

while [[ $COUNTER < $DURATION ]] ; do
  exhale
  inhale 
done

clear
echo "Well done ! See ya soon !"
