#!/usr/bin/python
import smtplib, ssl
from email.mime.text import MIMEText
from taskw import TaskWarrior

SMTP_DOMAIN=
PORT=
SENDER=
PASS=
FROM=
TO=

ctx = ssl.create_default_context()

w = TaskWarrior()
tasks = (w.load_tasks())['pending']

def isCourse (task):
    res = False
    if 'tags' in task:
        if "courses" in task["tags"]:
            print (task["description"])
            res = True
    return res

courses = filter(isCourse, tasks)

res = ""
j = 0
for i in courses:
   if j == 0: 
       res += ""  
   else: 
       res += "\n" 
   res += i["description"]
   j = j+1

print("sending fares")
try:
    server = smtplib.SMTP(SMTP_DOMAIN,PORT)
    server.starttls(context=ctx)
    server.ehlo()
    server.login(SENDER, PASS)
    mes = MIMEText(res)
    mes['Subject'] = "Liste de Courses"
    mes['From'] = FROM
    mes['To'] = TO
    server.sendmail(mes['From'], mes['To'], mes.as_string())
    print("sent")
except Exception as e:
    print(e)
finally:
    server.quit()
