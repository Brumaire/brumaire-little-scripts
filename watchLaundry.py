#!/usr/bin/env python
import os
import sys
import subprocess
import requests
import time
from bs4 import BeautifulSoup

if len(sys.argv) >= 4:
    registered = sys.argv[4]
else:
    registered = -1

UID = sys.argv[2]
MDP = sys.argv[3]
LOGIN_PAGE = "http://gad.lmcontrol.com/bienvenue"
DATA_PAGE = "http://gad.lmcontrol.com/controllers/client_requests_public.php"

client = requests.Session()

LOGIN_REQ = {
    'username_field':UID,
    'password_field':MDP,
}
loginResponse = client.post(LOGIN_PAGE, data=LOGIN_REQ)

if (registered == -1):
    dataResponse = client.get(DATA_PAGE)
    soup = BeautifulSoup(dataResponse.content, 'html.parser')
    machines_datas = soup.find(id = "info_message").find("tbody").find_all("td")
    for i in range(0,len(machines_datas),2):
        print(f"{machines_datas[i].text} : {machines_datas[i+1].text}")
    sys.exit(0)

done = False
while(not done):
    dataResponse = client.get(DATA_PAGE)
    soup = BeautifulSoup(dataResponse.content, 'html.parser')
    machines_datas = soup.find(id = "info_message").find("tbody").find_all("td")
    for i in range(0,len(machines_datas),2):
        if( registered in machines_datas[i].text and "Libre" in machines_datas[i+1].text):
            subprocess.run(["notify-send", "Lavage ou sêchage terminé", f"machine {registered}"])
            done = True
    if not done:
        time.sleep(150
