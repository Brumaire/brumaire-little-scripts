#!/bin/env bash

SERVICES_DELIM='!'
BOOKMARKS_DELIM='='

BOOKMARKS_LIST="$(cat $BOOKMARKS_FILE | cut -d $BOOKMARKS_DELIM -f 1)" &&
SERVICES_LIST="$(cat $SERVICES_FILE | cut -d $SERVICES_DELIM -f 1)" &&

lookupService() {
    echo "looking for a service named '$1' with params : '$2'"
    #$1 <-- ServiceName
    #$2 <-- param
    config=$(cat $SERVICES_FILE | grep -E "^$1!" | cut -d"$SERVICES_DELIM" -f 2-) &&
    serviceType=$(echo $config | cut -d ' ' -f 1) &&
    serviceLine=$(echo $config | cut -d ' ' -f 2- | sed "s#<PARAM>#$2#g")

    echo "found : $serviceType"

    if [[ $serviceType == "guiapp" ]] ; then
        echo "it is a graphical app"
        $serviceLine

    elif [[ $serviceType == "termapp" ]] ; then
        echo "it is a cli app"
        $TERMGRUN "$serviceLine"

    elif [[ $serviceType == "" ]] ; then
      echo "service not found !"
      return 1

    else 
        echo "this service seems to depends on another service, let's find which one"
        lookupService "$serviceType" "$serviceLine"
    fi
}

INPUT="$(echo "$BOOKMARKS_LIST" | rofi -show -dmenu - )" &&

BOOKMARK=$(cat $BOOKMARKS_FILE | grep -E "^$INPUT*" | rev | cut -d $BOOKMARKS_DELIM -f 1 | rev) &&

if [[ ! $INPUT ]] ; then
  exit 0

elif [[ $INPUT == *"$SERVICES_DELIM"* || $BOOKMARK == *"$SERVICES_DELIM"* ]] ; then
    #Is is a bookmark or unitary request
    if [[ $BOOKMARK == *"$SERVICES_DELIM"* ]] ; then
      QUERY="$BOOKMARK"
    else 
      QUERY="$INPUT"
    fi

    #service name
    SERVICENAME=$(echo $QUERY | cut -d "$SERVICES_DELIM" -f 2 | cut -d ' ' -f 1) &&

    #has a param ?
    if [[ $QUERY == *" "* ]] ; then
      PARAM=$(echo $QUERY | cut -d ' ' -f 2-)
    fi

    lookupService "$SERVICENAME" "$PARAM"
fi
